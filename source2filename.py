#!/usr/bin/env python
import sys
import xml.etree.ElementTree as ET
import lxml.etree
import re
import os.path




def convert_source(filename, prefix_path):
    #read input file
    fin = open(filename, "rt")
    #read file contents to string
    data = fin.read()
    # xmlstr is your xml in a string
    root = lxml.etree.fromstring(data)
    sources = root.find('sources')
    packages = root.find('packages')
    for package in packages:
        classes = package.find('classes')
        for clazz in classes:
            file_not_found = True
            for source in sources:
                full_filename = prefix_path + clazz.attrib['filename']
                print(full_filename)
                clazz.attrib['filename'] = full_filename

    data = lxml.etree.tostring(root, pretty_print=True)
    #close the input file
    fin.close()
    #open the input file in write mode
    fin = open(filename, "wb")
    #overrite the input file with the resulting data
    fin.write(data)
    #close the file
    fin.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: source2filename.py FILENAME")
        sys.exit(1)

    filename    = sys.argv[1]
    relative_path = sys.argv[2]
    convert_source(filename, relative_path)

