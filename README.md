# jacoco2cobertura

This container allow you to translate the tests instrumentation report - jacoco, to the expected input by gitlab -cobertura, original project from: https://gitlab.com/haynes/jacoco2cobertura

You can follow the discusion thread on https://gitlab.com/gitlab-org/gitlab/-/issues/217664#note_401391226

This works on Gitlab 13.9.2 however, the expected paths can change on the next versions, please feel free to open your MR.

the idea is to keep your sources field with absolute path
  <sources>
    <source>/builds/root/IsolationInstrumentationAndroid</source>
  </sources>

And your classes name as a relative paths, in this example the project its called  IsolationInstrumentationAndroid and has their MainActivity on the app folder.

        <class branch-rate="0.8125" complexity="13.0" filename="app/src/main/java/com/my/android/package/name/MainActivity.kt" line-rate="0.9444444444444444" name="com.my.android.package.name.MainActivity">
          <methods>
